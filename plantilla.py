#!/usr/bin/env pythoh3

'''
Program to order a list of words given as arguments
'''

import sys


def is_lower(first: str, second: str):
    """Return True if first is lower (alphabetically) than second

    Order is checked after lowercasing the letters
    `<` is only used on single characteres.
    """

def sort_pivot(words: list, pos: int):
    """Sorts word in pivot position, moving it to the left until its place"""


def sort(words: list):
    """Return the list of words, ordered alphabetically"""


def show(words: list):
    """Show words on screen, using print()"""


def main():
    words: list = sys.argv[1:]
    sort(words)
    show(words)

if __name__ == '__main__':
    main()
